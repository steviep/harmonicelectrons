<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <script type="text/javascript" async
          src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML">
  </script>
  <script src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="data.js"></script>
  <script src="app.js"></script>
</head>

<body>
  <p>
    The Schrodinger equation for two electrons trapped in a harmonic potential is,
    $$ \left[ -\frac{\hbar^2}{2m}\left(\nabla_1^2 + \nabla_2^2\right) + \frac12 k \left(r_1^2 + r_2^2\right)
    + \frac{e^2}{|\mathbf{r}_1 - \mathbf{r}_2|} - E \right] \Psi(\mathbf{r}_1, \mathbf{r}_2) = 0
    $$

  <p>
    We want to change coordinates to what we call the midpoint coordinate system, in which one vector,
    \(\mathbf{r}_+\) goes from the origin to the midpoint of the two electrons:

    $$ \mathbf{r}_+ = \frac{\mathbf{r}_1 + \mathbf{r}_2}{2}
    $$

    while the other vector, \(\mathbf{r}_-\), goes from the midpoint to electron 1:
    $$ \mathbf{r}_- = \frac{\mathbf{r}_1 - \mathbf{r}_2}{2}
    $$

    To get back to the conventional coordinate system, we have:

    $$ \mathbf{r}_1 = \mathbf{r}_+ + \mathbf{r}_-
    $$
    $$ \mathbf{r}_2 = \mathbf{r}_+ - \mathbf{r}_-
    $$

  <p>
    To transform Schrodinger's equation, we will need to use the chain rule:
    $$ \frac{\partial}{\partial{x_1}} = \frac{\partial {x_+}}{\partial{x_1}} \frac{\partial}{\partial{x_+}}
    + \frac{\partial{x_-}}{\partial{x_1}} \frac{\partial}{\partial{x_-}}
    = \frac12 \frac{\partial}{\partial{x_+}} + \frac12 \frac{\partial}{\partial{x_-}}
    $$

    And similarly,
    $$ \frac{\partial}{\partial x_2} = \frac{\partial x_+}{\partial x_2} \frac{\partial}{\partial x_+}
    + \frac{\partial x_-}{\partial x_2} \frac{\partial}{\partial x_-}
    = \frac12 \frac{\partial}{\partial x_+} - \frac12 \frac{\partial}{\partial x_-}
    $$

    Then it is straightforward to show
    $$ \frac{\partial^2}{\partial x_1^2} = \frac14 \frac{\partial^2}{\partial x_+^2}
    + \frac12 \frac{\partial^2}{\partial x_+ \partial x_-}
    + \frac14 \frac{\partial^2}{\partial x_-^2}
    $$
    
    and again, similarly
    $$ \frac{\partial^2}{\partial x_2^2} = \frac14 \frac{\partial^2}{\partial x_+^2}
    - \frac12 \frac{\partial^2}{\partial x_+ \partial x_-}
    + \frac14 \frac{\partial^2}{\partial x_-^2}
    $$

    So that we obtain the convenient result:
    $$ \left( \frac{\partial^2}{\partial x_1^2} + \frac{\partial^2}{\partial x_2^2}\right)
    = \frac12 \left( \frac{\partial^2}{\partial x_+^2} + \frac{\partial^2}{\partial x_-^2}\right)
    $$

    And, from similar results for the \(y\) and \(z\) partial derivitives, we obtain
    $$ \nabla_1^2 + \nabla_2^2 = \frac12 \left(\nabla_+^2 + \nabla_-^2 \right)
    $$

    Furthermore,
    $$ r_1^2 + r_2^2 = (\mathbf{r}_+ + \mathbf{r}_-) \cdot (\mathbf{r}_+ + \mathbf{r}_-)
    + (\mathbf{r}_+ - \mathbf{r}_-) \cdot (\mathbf{r}_+ - \mathbf{r}_-)
    $$
    $$ = (r_+^2 + 2\mathbf{r}_+ \cdot \mathbf{r}_- + r_-^2) + (r_+^2 - 2\mathbf{r}_+ \cdot \mathbf{r}_- + r_-^2)
    $$
    $$ = 2(r_+^2 + r_-^2)
    $$

    So our original equation becomes
    $$ \left[ - \frac{\hbar^2}{4m} \left( \nabla_+^2 + \nabla_-^2  \right)
     + k(r_+^2 + r_-^2) + \frac{e^2}{|\mathbf{r}_1 - \mathbf{r}_2|} - E
    \right] \Psi(\mathbf{r}_+, \mathbf{r}_-) = 0
    $$

    And, because \( |\mathbf{r}_1 - \mathbf{r}_2| = 2r_- \), we get,
    $$ \left[ - \frac{\hbar^2}{4m} \left( \nabla_+^2 + \nabla_-^2  \right)
     + k(r_+^2 + r_-^2) + \frac{e^2}{2r_-} - E
    \right] \Psi(\mathbf{r}_+, \mathbf{r}_-) = 0
    $$

    The classical angular frequency of a harmonic oscillator is given by
    $$ \omega = \sqrt{\frac km}
    $$

    Replacing \(k\) with \(m\omega^2\) gives
    $$ \left[ - \frac{\hbar^2}{4m} \left( \nabla_+^2 + \nabla_-^2  \right)
     + m\omega^2(r_+^2 + r_-^2) + \frac{e^2}{2r_-} - E
    \right] \Psi(\mathbf{r}_+, \mathbf{r}_-) = 0
    $$

    To convert to dimensionless coordinates, notice that
    $$ \sqrt{\frac{\hbar}{m\omega}} \sim \left( \frac{\textrm{gm cm}^2 \ \textrm{s}^{-1}}{\textrm{gm s}^{-1}}  \right)^{1/2}
    $$
    has units of length.
  <p>
    We change coordinates to dimensionless position vectors \(\mathbf{\rho}_\pm\) via
    $$ \mathbf{r}_\pm = \sqrt{\frac{\hbar}{2m\omega}} \mathbf{\rho}_\pm
    $$

    [The factor of 2 is included to make the resulting equations simpler]
  <p>
    Then we have these identites:
    
    $$ \nabla_\pm^2 = \frac{2m\omega}{\hbar} \nabla_{\rho_\pm}^2
    $$
    $$ r_+^2 + r_-^2 = \frac{\hbar}{2m\omega} (\rho_+^2 + \rho_-^2)
    $$
    $$ \frac{1}{r_-} = \sqrt{\frac{2m\omega}{\hbar}} \frac{1}{\rho_-}
    $$

    And the wave equation can be written as
    $$
    \left[
    - \left( \frac{\hbar^2}{4m}  \right)
     \left( \frac{2m\omega}{\hbar}  \right)
    \left( \nabla_{\rho_+}^2 + \nabla_{\rho_-}^2  \right)
    + \left( m\omega^2  \right) \left( \frac{\hbar}{2m\omega}  \right) (\rho_+^2 + \rho_-^2)
    + \left( \frac{e^2}{2}  \right) \sqrt{\frac{2m\omega}{\hbar}} \frac{1}{\rho_-}
    - E
    \right] \Psi = 0
    $$
  <p>
    Or, simplifying
    $$
    \left[
    - \frac{\hbar\omega}{2} \left( \nabla_{\rho_+}^2 + \nabla_{\rho_-}^2  \right)
    + \frac{\hbar\omega}{2} (\rho_+^2 + \rho_-^2)
    + \sqrt{\frac{m\omega}{2\hbar}} \frac{e^2}{\rho_-}
    - E
    \right] \Psi = 0
    $$
    Multiplying through by \(-2/\hbar\omega\), we arrive at
    $$
    \left[
    \nabla_{\rho_+}^2 + \nabla_{\rho_-}^2
    - \rho_+^2 - \rho_-^2
    - \sqrt{\frac{2m}{\hbar^3\omega}} \frac{e^2}{\rho_-}
    + \frac{2E}{\hbar\omega}
    \right] \Psi = 0
    $$
  <p>
    Defining the dimensionless parameter \(\lambda_e\) thusly
    $$ \lambda_e = \sqrt{\frac{2m}{\hbar^3\omega}} e^2
    $$
    and defining \(\epsilon\) to be the total energy in units of \(\hbar\omega\)
    $$ \epsilon = \frac{E}{\hbar\omega}
    $$
    we obtain the completely dimensionless equation:
    $$
    \left[
    \nabla_{\rho_+}^2 + \nabla_{\rho_-}^2
    - \rho_+^2 - \rho_-^2
    - \frac{\lambda_e}{\rho_-}
    + 2\epsilon
    \right] \Psi = 0
    \tag{1}
    $$
  <p>
    From this point forward, for convenience, we will replace \(\rho_\pm\) with \(r_\pm\),
    remembering that \(r\) is in units of \(\sqrt{\hbar/2m\omega}\)
    and \(\epsilon\) is in units of \(\hbar\omega\).
    Equation (1) becomes
    $$
    \left[
    \nabla_+^2 + \nabla_-^2
    - r_+^2 - r_-^2
    - \frac{\lambda_e}{r_-}
    + 2\epsilon
    \right] \Psi = 0
    $$

    which, by writing \(\Psi = \Psi_+(r_+) \Psi_-(r_-)\) can easily be separated into the two equations:

    $$
    \left[
    \nabla_+^2 - r_+^2 + 2\epsilon_+
    \right] \Psi_+ = 0
    $$
    and
    $$
    \left[
    \nabla_-^2 - r_-^2 - \frac{\lambda_e}{r_-} + 2\epsilon_-
    \right] \Psi_- = 0
    $$
    where \(\epsilon_+\) and \(\epsilon_-\) are constants such that
    $$ \epsilon_+ + \epsilon_- = \epsilon
    $$

  <p>
    The \(\Psi_+\) equation is the Schrodinger equation for a harmonic oscillator, which can be separated
    either into \(x\), \(y\), and \(z\) components, or in spherical coordinates.  The solutions are
    known, with energies given by

    $$ \epsilon_+ = \frac32 + n \tag{2}$$
    For \(n = 0, 1, ...\)
  <p>
    In rectangular coordinates (i.e. writing \(\Psi_+ = X(x)Y(y)Z(z)\)) the wave function \(X\) has the form

    $$ X(x) = C H_{n_x}(x) \exp\left(-\frac{x^2}{2}\right) $$

    where \(C\) is a normalization constant and
    the \(H_n\) are the Hermite polynomials, and the contribution to the total energy is \(1/2 + n_x\).
    Each of the other functions \(Y(y)\) and \(Z(z)\) will have the same form, with possibly different
    values of \(n_y\) and \(n_z\), and the total energy is the sum of the three separate energies,
    recovering the form given in equation {2}.

  <p>
    It is also possible to separate the \(\Psi_+\) equation in sperical coordinates, in which case the
    quantum numbers will be \(n\), \(\ell\), and \(m\).  We shall consider those solutions below.
  <p>

    All that is left is the equation for \(\Psi_-\).  We drop the subscript in the following.

    $$ \left[
    \nabla^2 - r^2 - \frac{\lambda_e}{r} + 2\epsilon
    \right] \Psi = 0
    $$

    In spherical coordinates, we have

    $$
    \nabla^2 = \frac{1}{r^2}\frac{\partial}{\partial{r}}\left(r^2 \frac{\partial}{\partial{r}} \right)
    + \frac{1}{r^2}\left[
    \frac{1}{\sin\theta} \frac{\partial}{\partial{\theta}} \left( \sin\theta \frac{\partial}{\partial{\theta}}  \right)
    + \frac{1}{\sin^2\theta} \frac{\partial^2}{\partial{\phi}^2}
    \right]
    $$

  <p>
    If we write the wave function as $$ \Psi(\mathbf{r}) = R(r)Y_{\ell m}(\theta, \phi) $$ then we can
    replace the angular operator in brackets with \(-\ell(\ell + 1)\), and we obtain the following equation,

    $$
    \left[
    \frac{1}{r^2}\frac{d}{dr}\left(r^2 \frac{d}{dr} \right)
    - \frac{\ell(\ell + 1)}{r^2} - r^2 - \frac{\lambda_e}{r} + 2\epsilon
    \right] R(r) = 0
    $$

  <p>
    For large \(r\), any solution must approach a linear combination of \(\exp(-r^2/2)\) and \(\exp(+r^2/2)\)
    but a physical solution must approach the former in order to be normalizable.  So, we write

    $$
    R(r) = \exp\left( - \frac{r^2}{2}  \right) f(r)
    $$

    We evaluate the derivatives in the equation, thusly

    $$
    \begin{align}
    R &= \exp\left( - \frac{r^2}{2}  \right) f \\
    \frac{dR}{dr} &= -r \exp\left( - \frac{r^2}{2}  \right) f + \exp\left( - \frac{r^2}{2}  \right) f' \\
    r^2 \frac{dR}{dr} &= -r^3 \exp\left( - \frac{r^2}{2}  \right) f + r^2 \exp\left( - \frac{r^2}{2}  \right) f' \\
    \frac{d}{dr} \left( r^2 \frac{dR}{dr}  \right) &= -3r^2 \exp\left( - \frac{r^2}{2}  \right) f + r^4 \exp\left( - \frac{r^2}{2}  \right) f -r^3 \exp\left( - \frac{r^2}{2}  \right) f' \\
    & \qquad\qquad\qquad + 2r \exp\left( - \frac{r^2}{2}  \right) f' - r^3 \exp\left( - \frac{r^2}{2}  \right) f' + r^2 \exp\left( - \frac{r^2}{2}  \right) f''\\
    &= -3r^2 \exp\left( - \frac{r^2}{2}  \right) f + r^4 \exp\left( - \frac{r^2}{2}  \right) f - 2r^3 \exp\left( - \frac{r^2}{2}  \right)f' + 2r \exp\left( - \frac{r^2}{2}  \right) f' + r^2 \exp\left( - \frac{r^2}{2}  \right) f'' \\
    \frac{1}{r^2} \frac{d}{dr} \left( r^2 \frac{dR}{dr}  \right) &= -3 \exp\left( - \frac{r^2}{2}  \right) f + r^2 \exp\left( - \frac{r^2}{2}  \right) f - 2r \exp\left( - \frac{r^2}{2}  \right)f' + \frac{2}{r} \exp\left( - \frac{r^2}{2}  \right) f' + \exp\left( - \frac{r^2}{2}  \right) f'' \\
    &= \exp\left( - \frac{r^2}{2}  \right) \left[ (r^2 - 3) f + \left( \frac{2}{r} - 2r  \right) f' + f''
    \right]
    \end{align}
    $$

    Filling in the rest of the equation,

    $$
    \left[
    - \frac{\ell(\ell + 1)}{r^2} - r^2 - \frac{\lambda_e}{r} + 2\epsilon
    \right] R = \exp\left( - \frac{r^2}{2}  \right) \left[ - \frac{\ell(\ell+1)}{r^2} -r^2 - \frac{\lambda_e}{r} + 2\epsilon \right] f
    $$

    Our equation becomes, after factoring out the exponential term,

    $$
    \left[ (-3 - \frac{\ell(\ell+1)}{r^2} - \frac{\lambda_e}{r} +2\epsilon \right] f
    + \left( \frac{2}{r} - 2r  \right) f' + f'' = 0
    $$

    Multiplying through by \(r^2\), we get
    
    $$
    \left[ (2\epsilon - 3)r^2 - \lambda_e r - \ell(\ell+1)  \right] f + (2r - 2r^3) f' + r^2 f'' = 0
    $$

    We write the function \(f\) as a power series in \(r\).
    $$ f(r) = \sum_{n=0}^\infty a_n r^n
    $$

    We have

    $$
    \begin{align}
    f &= \sum_{n=0}^\infty a_n r^n \\
    f' &= \sum_{n=0}^\infty n a_n r^{n-1} \\
    f'' &= \sum_{n=0}^\infty n (n-1) a_n r^{n-2}
    \end{align}
    $$

    Plugging these into our equation, we get

    $$
    \sum_{n=0}^\infty (2\epsilon - 3) a_n r^{n+2} - \lambda_e a_n r^{n+1} - \ell(\ell - 1) a_n r^n
    + 2 n a_n r^n - 2 n a_n r^{n+2} + n(n-1) a_n r^n = 0
    $$

    Collecting like terms, we get

    $$
    \sum_{n=0}^\infty (2\epsilon - 3 - 2n) a_n r^{n+2} - \lambda_e a_n r^{n+1} + [n(n-1) + 2n - \ell(\ell + 1)] a_n r^n = 0
    $$

    Which can be slightly simplified to

    $$
    \sum_{n=0}^\infty (2\epsilon - 3 - 2n) a_n r^{n+2} - \lambda_e a_n r^{n+1} + [n(n+1) - \ell(\ell + 1)] a_n r^n = 0
    \tag{3}
    $$

  <p>
    Calculating the coefficient of a particular power of \(r\), say \(r^j\), in the left hand side, we
    obtain

    $$
    \left[2\epsilon - 3 - 2(j-2)\right] a_{j-2} - \lambda_e a_{j-1} + \left[j(j+1) - \ell(\ell+1)\right] a_j
    $$

    or

    $$
    (2\epsilon -2j + 1) a_{j-2} - \lambda_e a_{j-1} + \left[j(j+1) - \ell(\ell+1)\right] a_j
    $$

    This is strictly valid only for \(j \ge 2 \), but if we insist that all \(a_n\) vanish for negative \(n\),
    then it is true for all \(j\).

    Each coefficient of \(r^j\) must vanish for the summation in equation (3) to equal zero.  Thus we get the recursion formula
    for the \(a_n\)'s

    $$
    \left[j(j+1) - \ell(\ell+1)\right] a_j = \lambda_e a_{j-1} + (2j - 1 - 2\epsilon) a_{j-2}
    $$

  <p>
    Not all the \(a_n\)'s can be zero, so
    let \(k\) be the smallest index for which \(a_k\) is nonzero.  The recursion formula at \(j=k\) is

    $$
    \left[k(k+1) - \ell(\ell+1)\right] a_k = \lambda_e a_{k-1} + (2k - 1 - 2\epsilon) a_{k-2}
    $$

    But, by assumption, the right hand side is zero. Therefore, the factor in front of \(a_k\) must also
    be zero, implying \(k = \ell\).  [This is as expected if we examine the original wave equation for \(R\)
    at very small values of \(r\).  Indeed, many developments will include the factor \(r^\ell\) in \(R\)
    from the outset]

  <p>
    Thus, we have,

    $$
    a_n = \frac{\lambda_e a_{n-1} + (2n - 2\epsilon - 1) a_{n-2}}{n(n+1) - \ell(\ell+1)}
    $$
    
    for all \(n > \ell\).

  <p>
    If we take \(\lambda_e\) to be zero in this formula, then we recover the purely harmonic oscillator,
    and we see that only even powers of \(r\) will appear when \(\ell\) is even, and
    only odd powers when it is odd.  Furthermore, the power series must end, or else
    the function \(f(r)\) will grow as \(\exp(r^2)\).  This requires the factor \(2n - 2\epsilon - 1\)
    be zero for some value \(n\) greater than \(\ell\), but of the same parity.  The possible values of \(n\) are
    therefore \(\ell + 2 + 2k\)
    for some integer \(k \ge 0\).  Thus we have

    $$
    2\epsilon = 2(\ell + 2 + 2k) - 1
    $$

    $$
    \epsilon = \frac32 + \ell + 2k
    $$

    which produces the same energies of the previous result, equation (2).

  <p>
    In the case where \(\lambda_e\) is not zero, we have a difficult problem.
    There will be both even and odd powers of \(r\).  For very large indices, the
    recursion formula approaches
    $$ a_{n+2} = \frac{2}{n} a_n \tag{4}$$

    and so the even powers become approximately decoupled from the odd powers.  Both of these series
    blow up like \(\exp(r^2)\), but it is possible that the even and odd coefficients are of opposite
    sign, so we must find some means of determining, for a given value of energy \(\epsilon\), when
    the function \(f\) blows up positive or negative.

  <p>
    To this end, let us take the asymptotic recursion formula (4) as exact.
    The general solution, \(f(r)\) splits into two series, an even series, which is entirely
    determined by \(a_0\), and an odd series entirely determined by \(a_1\).
  <p>
    The first few even coefficients are as follows:
    $$ a_2 = \frac{2}{2} a_0 = a_0 $$
    $$ a_4 = \frac{2}{4} a_2 = \frac{1}{2} a_2 = \frac{1}{2} a_0 $$
    $$ a_6 = \frac{2}{6} a_4 = \frac{1}{2\times3} a_0 $$
    $$ a_8 = \frac{2}{8} a_6 = \frac{1}{2\times 3\times 4} a_0 $$
    And the general even coefficient is (taking \(n = 2k\)):
    $$ a_{2k} = \frac{1}{k!} a_0 $$

  <p>
    For the odd coefficients we get something more involved
    $$ a_3 = \frac{2}{3} a_1 $$
    $$ a_5 = \frac{2}{5} a_3 = \frac{2\times 2}{5 \times 3} a_1 $$
    $$ a_7 = \frac{2}{7} a_5 = \frac{2\times 2\times 2}{7 \times 5 \times 3} a_1 $$
    So that the general odd term is (taking \(n = 2k+1\)):
    $$ a_{2k+1} = \left(\frac{2}{2k+1}\right)\left(\frac{2}{2k-1}\right)\left(\frac{2}{2k-3}\right)...\left(\frac{2}{3}\right) a_1 $$
  <p>
    But we re-write the fractions thusly:

    $$ a_{2k+1} = \left(\frac{1}{\frac{2k+1}{2}}\right) \left(\frac{1}{\frac{2k-1}{2}}\right) \left(\frac{1}{\frac{2k-3}{2}}\right) ... \left(\frac{1}{\frac{3}{2}}\right) a_1$$
    or
    $$ a_{2k+1} = 
    \dfrac{a_1}{\left( k + \frac12 \right) \left(k + \frac12 - 1 \right) \left(k + \frac12 - 2\right) ... \left(\frac32\right)} \tag{5}$$
  <p>

    Using the recursion formula of the Gamma function \(\Gamma(z+1) = z \Gamma(z)\), and applying it \(k\) times, we get
    $$ \Gamma(k+z+1) = (k+z)(k+z-1)(k+z-2)...(z+1) \Gamma(z+1) $$
    And if we take \(z = \frac{1}{2}\) we get
    $$ \Gamma\left(k+\frac{3}{2}\right) = \left(k + \frac{1}{2}\right)\left(k + \frac{1}{2} - 1 \right)\left(k + \frac{1}{2} - 2 \right) ... \left(\frac{3}{2}\right) \Gamma\left(\frac{3}{2}\right) $$
    So, we can write the denominator in equation (5) as
    $$ \frac{\Gamma\left(k + \frac{3}{2}\right)}{\Gamma\left(\frac{3}{2}\right)} $$

  <p>
    The general odd coefficient can therefore be written as

    $$ a_{2k+1} = \frac{\Gamma\left(\frac{3}{2}\right)}{\Gamma\left(k + \frac{3}{2}\right)} a_1 $$
    And the completely general solution can be written
    $$ f(r) = a_0 \sum_{k=0}^{\infty} \frac{r^{2k}}{k!}  +
    a_1 \Gamma\left(\frac{3}{2}\right) \sum_{k=0}^\infty \frac{r^{2k+1}}{\Gamma\left(k + \frac{3}{2}\right)} $$

    which, upon defining \(\alpha\) to be \(a_0\) and \(\beta\) to be \(-a_1\),
    becomes
    $$ f(r) = \alpha \sum_{k=0}^{\infty} \frac{(r^2)^k}{\Gamma(k+1)}  -
    \beta \: \Gamma\left(\frac{3}{2}\right) \sum_{k=0}^\infty \frac{(r^2)^{k+\frac12}}{\Gamma\left(k + \frac12 + 1\right)} $$

    The even series is equal to \(e^{r^2}\), and the odd series also blows up at a similar rate.
  <p>
    Either series, by itself, will cause \(R(r) = e^{-\frac{r^2}{2}} f(r)\) to blow up.
    
    We want to find a condition between \(\alpha\) and \(\beta\) such that
    \(R(r)\) goes to zero as \(r \to \infty\).  When \(\alpha = 1\) and \(\beta = 0\), clearly \(R\) blows up positive.
  <p>
    Numerically, I fixed \(\alpha\) at 1 and tested \(R(r)\) for various choices of \(\beta\).  I discovered that \(R(r)\) also
    blows up positive when \(\beta = 1\), but goes negative when \(\beta = 2\).  I did a binary search and discovered that \(R\)
    changes sign when \(\beta\) is about 1.12837.  I discovered that this value satisfies the equation
    $$\beta \: \Gamma\left(\frac{3}{2}\right) = 1 $$
    
  <p>
    Given that the value of the Gamma function at \(3/2\) is \(\sqrt{\pi}/2\), the value of \(\beta\) where \(R\)
    becomes normalizable is \(\beta = 2/\sqrt\pi\).

    Below is a plot of both \(f\) and \(f e^{-\frac{r^2}{2}}\) for the two values of \(\beta\) closest
    to \(2/\sqrt\pi\).  The two values differ in the least significant bit of a Java double.  The lower
    value of \(\beta\) is labeled as f1 and the barely higher value as f2.  As can be seen from the
    graph, f1 and f2 are effectively equal until somewhere between 5 and 6 units, when they diverge in
    opposite directions.

    
  <div id="chart"
       style="border:1px inset black;padding:0;width:80%;height:600px">
  </div>

  <p>
    From this graph, it appears that the exact function, where \(\beta = 1/\Gamma(3/2)\), which
    has the form

    $$
    f(r) = \sum_{n=0}^{\infty} \frac{(-1)^n}{\Gamma(n/2 + 1)} r^n
    $$

    asymptotically goes to zero.
    Note that this is not required for \(R\) to converge.  The Hermite polynomials, other than \(H_0\)
    grow infinitely (either positive or negative) as \(r \to \infty\).  But when multiplied by
    the exponential term \(\exp(-r^2/2)\), the resulting wave function is normalizable.

  <p>
    This result allows us to determine whether \(R\) blows up positive or negative, from the ratios of
    successive coefficients \(a_n\), where \(n\) is large enough that the recursion formula can be taken
    to be the decoupled one of equation (4).
  <p>
    The energy eigenvalues can then be determined by searching for where the asymptotic behavior changes
    sign.

</body>
</html>
