google.charts.load('current', {packages:['corechart']});
google.charts.setOnLoadCallback(function() {drawChart();});

function drawChart() {
  var data = [
    ['r', 'n=1', 'n=2', 'n=3']
  ];

  for (var i=0; i<n1l0.length; i++) {
    var cols = [n1l0[i][0], n1l0[i][2], n2l0[i][2], n3l0[i][2]];
    data.push(cols);
  }

  var materialOptions = {
    series: {
      // Gives each series an axis name that matches the Y-axis below.
      0: {axis: 'f'},
      1: {axis: 'f*exp'}
    },
    axes: {
      // Adds labels to each axis; they don't have to match the axis names.
      y: {
        Temps: {label: 'Temps (Celsius)'},
        Daylight: {label: 'Daylight'}
      }
    }
  };
  var chart = new google.visualization.LineChart(document.getElementById('chart'));
  var gdata = google.visualization.arrayToDataTable(data);
  chart.draw(gdata, {});
}
