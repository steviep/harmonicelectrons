package throwaway;

public class Recurse {
    private double[] a = new double[4];
    private int j;
    private int exponent;
    private double maxValue = 1.0E10;
    
    public void init(int j) {
        a[0] = 0.0;
        a[1] = 0.0;
        a[2] = 0.0;
        a[3] = 0.0;
        a[j] = 1.0;
        exponent = 0;
        this.j = 4;
    }
    
    public void scale_a(int power) {
        double scale = Math.pow(10.0, -power);
        exponent += power;
        for (int i=0; i<4; i++) {
            a[i] *= scale;
        }
    }
    
    public void shiftIn(double val) {
        for (int i=3; i>=1; i--) {
            a[i] = a[i-1];
        }
        a[0] = val;
        if (val > maxValue) {
            scale_a(10);
        }
    }
    
    public String printRatios() {
        double max = 0;
        for (double val : a) {
            max = Math.max(max, Math.abs(val));
        }
        StringBuffer b = new StringBuffer();
        for (int i=0; i<4; i++) {
            b.append(a[i]/max);
            if (i<3) b.append(", ");
        }
        return b.toString();
    }
    
    public String printa() {
        StringBuffer b = new StringBuffer();
        double maxlog = 0;
        for (double val : a) {
            double log10 = Math.log10(Math.abs(val));
            maxlog = Math.max(log10, maxlog);
        }
        int powerOf10 = (int) maxlog;
        double scale = Math.pow(10, -powerOf10);
        double sum = 0;
        for (int i=0; i<4; i++) {
            double value = a[i] * scale;
            sum += value;
            b.append(value);
            b.append(", ");
        }
        b.append("sum:" + sum);
        b.append(" *10** " + (exponent + powerOf10));
        return b.toString();
    }
    
    public void iterate() {
        double value = 4*a[0] - 6*a[1] + 4*a[2] - a[3];
        shiftIn(value);
        j++;
        /*
        if ((j%100) == 0)
            System.out.println("j:" + j + ", a:" + printa());
        */
    }
    
    public void doIterations(int n, int index) {
        init(index);
        System.out.println("initial values are " + a[0] + ", " + a[1] + ", " + a[2] + ", " + a[3]);
        for (int i=0; i<n; i++)
            iterate();
        for (int i=0; i<8; i++) {
            iterate();
            System.out.println("j:" + j + ", a:" + printa());
            System.out.println(printRatios());
        }
    }

    public static void main(String[] args) {
        Recurse r = new Recurse();
        for (int j=0; j<4; j++)
            r.doIterations(10000,j);
    }
}
