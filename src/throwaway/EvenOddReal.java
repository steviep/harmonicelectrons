package throwaway;

import com.nasmas.precision.Real;

public class EvenOddReal {
    private Real[] a_n = new Real[4000];
    
    private void populate() {
        Real beta = new Real(-2).div(Real.PI().sqrtNewton());
        Real two = new Real(2);
        // System.out.println("two over root pi is :" + beta);
        a_n[0] = new Real(1);
        a_n[1] = beta;
        for (int n=2; n < a_n.length; n++) {
            Real n_r = new Real(n);
            a_n[n] = two.mul(a_n[n-2].div(n_r));
        }
    }
    
    public Real evaluate(Real r) {
        Real result = a_n[0];
        Real rToTheN = new Real(1);
        for (int n=1; n<a_n.length; n++) {
            rToTheN = rToTheN.mul(r);
            result = result.add(rToTheN.mul(a_n[n]));
        }
        return result;
    }
    public void plot() {
        populate();
        Real upper = new Real(20);
        Real incr = new Real(1);
        Real divisor = new Real(16);
        incr = incr.div(divisor);
        for (Real r = new Real(0); r.LE(upper); r = r.add(incr)) {
            Real f = evaluate(r);
            System.out.println("[" + r + "," + f + "],");
        }
    }
    
    public static void main(String[] args) {
        System.out.println("var usingreal = [");
        Real.setSignificant32s(10);
        Real.setDigitsToPrint(4);
        EvenOddReal obj = new EvenOddReal();
        obj.plot();
        System.out.println("];");
    }

}
