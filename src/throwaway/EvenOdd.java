package throwaway;

public class EvenOdd {

    private double beta;
    private double beta_lower;
    private double beta_upper;
    private double delta;
    private boolean printit = false;
    
    public double evaluate_f(double r) {
        double result = 0;
        double a = 1;
        double b = beta;
        double n = 0;
        double r_n = 1;
        while (true) {
            result += a * r_n + b * r * r_n;
            n += 2;
            r_n *= r*r;
            a = 2*a/n;
            b = 2*b/(n+1);
            if (a == 0 && b == 0) break;
        }
        return result * Math.exp(-r*r/2);
    }
    
    private static double two_over_root_pi = 2.0 / Math.sqrt(Math.PI);
    private double b(int k) {
        double result = 1.0;
        for (int i = 2*k; i >= 2; i -= 2) {
            result *= (1.0 * i) / (1.0 * (i+1));
        }
        return result * two_over_root_pi;
    }
    
    public void plot_f2() {
        delta = 1/32.0;
        System.out.println("var rawdata = [");
        for (double r=0; r<6.5; r += delta) {
            double r_2 = r*r;
            double b_k = 2.0 / Math.sqrt(Math.PI);
            double funcValue = (1.0 - b_k * r);
            double r_2k_over_fact = 1.0;
            for (int k=1; k<10000; k++) {
                b_k = b(k);
                r_2k_over_fact *= r_2 / k;
                funcValue += (1.0 - b_k * r) * r_2k_over_fact;
            }
            System.out.println("[" + r + ", " + funcValue + "],");
        }
    }
    
    public double plot_f() {
        double result = 0;
        if (printit)
            System.out.println("var data = [");
        for (double r = 0; r < 100; r += delta) {
            double f = evaluate_f(r);
            if (Double.isNaN(f) || Double.isInfinite(f))
                break;
            result = f;
            if (printit) {
                double exp = Math.exp(-r*r/2);
                double ratio = f/exp;
                System.out.print("[" + r + ", " + f);
                System.out.print("," + ratio);
                System.out.println("],");
            }
        }
        if (printit)
            System.out.println("];");
        return result;
    }
    
    public void init(double beta, double delta) {
        this.beta = beta;
        this.delta = delta;
    }
    
    public double f_large(double beta) {
        init(beta, .1);
        return plot_f();
    }
    
    public void binary_search(double beta1, double beta2) {
        double sign1 = Math.signum(f_large(beta1));
        //double sign2 = Math.signum(f_large(beta2));
        //System.out.println("initially sign1:" + sign1 + ", sign2:" + sign2);
        for (int i=0; i< 100; i++) {
            // System.out.println("beta1:" + beta1 + ", beta2:" + beta2);
            double beta = 0.5 * (beta1 + beta2);
            double testval = Math.signum(f_large(beta));
            if (testval == sign1)
                beta1 = beta;
            else
                beta2 = beta;
        }
        this.beta_lower = beta1;
        this.beta_upper = beta2;
    }
    
    
    public void find_beta() {
        binary_search(-1.126, -1.131);
        printit = true;
        delta = 1.0/16.0;
        beta = beta_lower;
        plot_f();
        beta = beta_upper;
        plot_f();
        System.out.println("beta lower:" + Math.abs(beta_lower) + " = " + Double.toHexString(Math.abs(beta_lower)));
        System.out.println("beta upper:" + Math.abs(beta_upper) + " = " + Double.toHexString(Math.abs(beta_upper)));
    }
    
    public static void main(String[] args) {
        EvenOdd obj = new EvenOdd();
        // obj.find_beta();
        obj.plot_f2();
    }
}
