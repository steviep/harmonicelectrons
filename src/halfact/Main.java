package halfact;

public class Main {
    public double exp_half_r2(double r) {
        return Math.exp(-r*r/2);
    }
    
    private double lambda_e = 2.0;
    private double e = 1.0;
    private int l = 0;
    
    private double[] a;
    private double[] b;
    private int len;
    private int J;
    private double limit = Math.pow(2.0, 100);
    
    public void init() {
        a = new double[len+4];
        b = new double[len+4];
        J = len - 1;
    }
    
    public void rescale(double[] arr, double scale) {
        for (int i=0; i<arr.length; i++)
            arr[i] *= scale;
    }
    
    public double scale_value(double[] arr, double val) {
        while (Math.abs(val) > limit) {
            double scale = 1.0 / limit;
            val *= scale;
            rescale(arr, scale);
        }
        return val;
    }
    
    public void set_a_sub(int n, double val) {
        a[n+2] = scale_value(a, val);
    }
    
    public void set_b_sub(int n, double val) {
        b[n+2] = scale_value(b, val);
    }
    
    
    public double a_sub(int n) {
        return a[n+2];
    }
    
    public double b_sub(int n) {
        return b[n+2];
    }
    
    public double value(double[]arr, int n) {
        double val = lambda_e * arr[n+3] - ((n+2)*(n+3) - l*(l+1)) * arr[n+4];
        val /= (e - 3 - 2*n);
        return val;
    }
    
    public void fill() {
        for (int n = J; n >= l-2; n--) {
            set_a_sub(n, value(a, n));
            set_b_sub(n, value(b, n));
            keep_linear_independence(n);
        }
    }
    
    private void keep_linear_independence(int j) {
        double a_sub_j_factor = a_sub(j) * b_sub(j+1);
        double b_sub_j_factor = b_sub(j) * a_sub(j+1);
        double ratio = (a_sub_j_factor - b_sub_j_factor) / b_sub_j_factor;
        System.out.println("l.d: at j:" + j + " is " + ratio);
    }

    public void find_e(int len, double lower_e, double upper_e) {
        this.len = len;
        init();
        set_a_sub(J+1, 1.0);
        set_a_sub(J+2, 0.0);
        set_b_sub(J+1, 0.0);
        set_b_sub(J+2, 1.0);
        for (e = lower_e; e < upper_e; e += 0.1) {
            fill();
            System.out.println("e:" + e + ", a_sub(l-1):" + a_sub(l-1) + ", a_sub(l-2):" + a_sub(l-2)
                + ", b_sub(l-1):" + b_sub(l-1) + ", b_sub(l-2):" + b_sub(l-2)
            );
            double c = - a_sub(l-1) / b_sub(l-1);
            double a_sub_l_1 = a_sub(l-1);
            double b_sub_l_1 = b_sub(l-1);
            double coeff = a_sub_l_1 + c * b_sub_l_1;
            coeff = a_sub(l-2) + c * b_sub(l-2);
            System.out.println(" c_sub(l-2):" + coeff);
        }
    }
    
    public static void main(String[] args) {
        Main m = new Main();
        m.find_e(100, 2.0, 6.0);
    }
}
