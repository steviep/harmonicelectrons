package org.gas;

public class CoefficientGenerator {
    private double an_1, an_2;
    private double lambda_e;
    private int l;
    private int l2;
    private int n;
    private double energy;
    private double ratio;
    private double betafactor;
    private double oddSignum;
    
    public CoefficientGenerator(int l, double lambda_e) {
        this.l = l;
        this.l2 = l*(l+1);
        this.n = l;
        this.lambda_e = lambda_e;
        double v_min = 3.0 * Math.pow(lambda_e / 2.0, 2.0/3.0);
        this.energy = v_min;
    }
    
    public double getEnergy() {
        return energy;
    }
    
    public void setEnergy(double energy) {
        this.energy = energy;
        an_1 = 1.0;
        an_2 = 0.0;
        this.n = l;
        // find an odd index greater than l
        int oddIndex = (l/2) * 2 + 1;
        if (oddIndex == l) oddIndex += 2;
        betafactor = 1.0;
        for (int i=3; i<=oddIndex; i +=2) {
            double id = i;
            betafactor *= id/(id-1);
        }
    }
    
    public int getIndex() {
        return this.n;
    }
    
    public double getBeta() {
        return -betafactor * ratio;
    }
    
    private static double criticalBeta = 2.0 / Math.sqrt(Math.PI);
    
    public double getBlowUp() {
        if (ratio > 0)
            return Math.signum(an_1);
        return (getBeta() - criticalBeta) * oddSignum;
    }
    
    public void scale(double scale) {
        an_1 *= scale;
        an_2 *= scale;
    }
    
    public double next() {
        n++;
        double an = lambda_e * an_1 + (2*n - energy - 1) * an_2;
        an /= n*(n+1) - l2;
        if (n % 2 == 1) {
            this.ratio = an / an_1;
            this.oddSignum = Math.signum(an);
            double nd = n;
            if (n > 1)
                this.betafactor *= nd / (nd-1);
        }
        an_2 = an_1;
        an_1 = an;
        return an;
    }
}
