package org.gas;

import java.io.FileWriter;
import java.io.IOException;

public class Schrodinger {
    private CoefficientGenerator gen;
    private double[] a_n;
    private FileWriter wrt;
    
    public Schrodinger(int l, double lambda_e) {
        gen = new CoefficientGenerator(l, lambda_e);        
        a_n = new double[2001];
    }
    
    private double evalFunc(double r) {
        double func = a_n[0];
        double r_n = 1.0;
        for (int i=1; i<a_n.length; i++) {
            r_n *= r;
            if ( ! Double.isFinite(r_n)) break;
            double fact = a_n[i] * r_n;
            if ( ! Double.isFinite(fact)) break;
            double newsum = func + fact;
            if ( ! Double.isFinite(newsum)) break;
            func = newsum;
        }
        return func;
    }
    
    private void println(String s) {
        try {
            wrt.write(s + "\n");
        } catch (IOException e) {
            throw new RuntimeException("writing to file", e);
        }
    }
    
    public void plotFunc(String filename, double rmax, double incr) {
        wrt = null;
        try {
        wrt = new FileWriter(filename + ".js");
        println(filename + "=[");
        for (double r=0; r<=rmax; r+=incr) {
            double value = evalFunc(r);
            println("  [" + r + "," + value + "," + value*Math.exp(-r*r/2.0) + "],");
        }
        println("]");
        } catch(Exception e) {
            throw new RuntimeException("Error writing file " + filename, e);
        }
        finally {
            if (wrt != null) {
                try { wrt.close();} catch(Exception e) { }                 
            }
        }
    }
    
    private double testEnergy(double energy) {
        gen.setEnergy(energy);
        for (int i=0; i<a_n.length; i++) a_n[i]=0;
        a_n[0] = 1.0;
        int n;
        for (n=1; n<a_n.length; n++) {
            double next = gen.next();
            a_n[n] = next;
            if (Math.abs(next) < 1.0e-100) break;
        }
        for (; n<a_n.length; n++) {
            double next = gen.next();
            if (Math.abs(next) < 1.0e-100) gen.scale(1.0e200);
        }
        double asymptote = gen.getBlowUp();
        System.out.println("energy: " + energy + ", blows up:" + asymptote);
        return asymptote;
    }
    
    private double search(double loEnergy, double hiEnergy, double incr) {
        double asymptote = 0;
        double previousAsymptote = 0;
        boolean firstIter = true;
        double previousEnergy = 0;
        for (double energy = loEnergy; energy < hiEnergy; energy += incr) {
            asymptote = this.testEnergy(energy);
            if (firstIter || Math.signum(asymptote) == Math.signum(previousAsymptote)) {
                // have not found a change in sign
                previousEnergy = energy;
                previousAsymptote = asymptote;
                firstIter = false;
            }
            else {
                // found a change in sign
                return binarySearch(previousEnergy, Math.signum(previousAsymptote), energy);
            }
        }
        return 0;
    }
    
    private double binarySearch(double loEnergy, double loSignum, double hiEnergy) {
        double previousLo = 0;
        double previousHi = 0;
        double energy = 0;
        int maxIters = 100;
        int iter = 0;
        while (previousLo != loEnergy || previousHi != hiEnergy) {
            energy = (hiEnergy + loEnergy) / 2.0;
            previousHi = hiEnergy;
            previousLo = loEnergy;
            double asymptote = testEnergy(energy);
            if (Math.signum(asymptote) == loSignum) {
                loEnergy = energy;
            }
            else {
                hiEnergy = energy;
            }
            iter++;
            if (iter > maxIters) break;
        }
        return energy;
    }
    
    public static void main(String[] args) {
        double lambda_e = 2.0 * Math.sqrt(2.0);
        Schrodinger equation = new Schrodinger(0, lambda_e);
        double energy = equation.search(4.0, 8.0, 0.1);
        System.out.println("energy: " + energy);
        equation.plotFunc("n1l0", 5.0, .125);
        energy = equation.search(energy+.5, 100, .1);
        System.out.println("energy: " + energy);
        equation.plotFunc("n2l0", 5.0, .125);
        energy = equation.search(energy+.5, 100, .1);
        System.out.println("energy: " + energy);
        equation.plotFunc("n3l0", 5.0, .0625);
    }
    
}
