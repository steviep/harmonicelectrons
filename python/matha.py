# grammar
# expr = summation | '[' summation ']' [ '/' '[' summation ']' ]
# summation = term [ '+'|'-' term ]...
# term = [ '-' ] factor [ '*' factor]...
# factor = val [ ^ const ] | '(' summation ')' [ ^ const ]
# val = const | var | funcall
# const = ['-'] n [ / n]
# n = digit [ digit ]...
# var = {lowercase} [ {lowercase} | digit ]...
# funcall = FUNC | FUNC '(' varlist ')'
# FUNC = {uppercase} [ {uppercase} | digit ]...
# varlist = var [ ',' var ]...

import re
from fractions import Fraction

var_re = re.compile(" *([a-z][0-9]?)")
const_re = re.compile(' *(\d+(/\d+)?)')
minus_re = re.compile(' *([-])')
plus_or_minus_re = re.compile(' *([+-])')
exponent_re = re.compile(' *(\^)')
multiply_re = re.compile(' *([*])')
openparen_re = re.compile(' *(\()')
closeparen_re = re.compile(' *(\))')
openbracket_re = re.compile(' *(\[)')
closebracket_re = re.compile(' *(\])')
comma_re = re.compile(' *(,)')
funcname_re = re.compile(' *([A-Z][A-Z0-9]*)')
slash_re = re.compile(' *(/)')

VAR = 'VAR'
CONST = 'CONST'
FUNCALL = 'FUNCALL'
EXPRESSION = 'EXPRESSION'
FUNCALL = 'FUNCALL'
FACTOR = 'FACTOR'
TERM = 'TERM'
SUMMATION = 'SUMMATION'

class Expression:
    def __init__(self):
        self.type = EXPRESSION
        self.numerator = None
        self.denominator = None

    def __str__(self):
        result = ''
        if self.denominator:
            result = f'({self.numerator})/({self.denominator})'
        else:
            result = f'{self.numerator}'
        return result

    #Expression
    def deep_copy(self):
        expr = Expression()
        if self.numerator:
            expr.numerator = self.numerator.deep_copy()
        if self.denominator:
            expr.denominator = self.denominator.deep_copy()
        return expr

    #Expression
    def expand(self):
        self.numerator.expand()
        if self.denominator:
            self.denominator.expand()

    #Expression
    def simplify(self):
        self.numerator.simplify()
        if self.denominator:
            self.denominator.simplify()

    #Expression
    def collect(self):
        self.simplify()
        self.numerator.collect()
        if self.denominator:
            self.denominator.collect()
        

class Summation:
    def __init__(self):
        self.type = SUMMATION
        self.terms = []
    def __str__(self):
        if len(self.terms) == 0:
            print(f'should never happen')
            return '0'
        result = ''
        need_positive_sign = False
        for term in self.terms:
            result += term.str(need_positive_sign)
            need_positive_sign = True
        return result
    
    #Summation
    def deep_copy(self):
        summation = Summation()
        summation.terms = [term.deep_copy() for term in self.terms]
        return summation

    #Summation
    def collect(self):
        # collect like terms
        # make copies since some terms will be mutated
        terms = [term.deep_copy() for term in self.terms]
        for term in terms:
            term._deleted = False
        for i1,term1 in enumerate(terms):
            for i2 in range(i1+1, len(terms)):
                term2 = terms[i2]
                if term1._deleted or term2._deleted:
                    continue
                islike = term1.like(term2)
                if islike == None:
                    continue
                newconst = islike
                if newconst == Const.zero():
                    term1._deleted = True
                    term2._deleted = True
                else:
                    term1.set_const(newconst)
                    term2._deleted = True
        self.terms = [term for term in terms if not term._deleted]

    #Summation
    def expand(self):
        new_terms = []
        for term in self.terms:
            terms = term.expand_sums()
            if terms:
                new_terms += terms
            else:
                new_terms.append(term)
        self.terms = new_terms

    #Summation
    def simplify(self):
        for term in self.terms:
            term.simplify()

class Term:
    def __init__(self):
        self.type = TERM
        self.sign = 1
        self.factors = []

    def str(self, include_sign=False):
        result = ''
        if self.sign < 0:
            result += '-'
        elif include_sign:
            result += '+'
        factors = [f'{factor}' for factor in self.factors]
        return result + '*'.join(factors)

    def __str__(self):
        return self.str(True)

    #Term
    def deep_copy(self):
        term = Term()
        term.sign = self.sign
        term.factors = [factor.deep_copy() for factor in self.factors]
        return term

    #Term
    def simplify(self):
        consts = []
        vars = []
        funcs = []
        sums = []
        for fact in self.factors:
            if fact.val.type == CONST:
                consts.append(fact)
            elif fact.val.type == VAR:
                vars.append(fact)
            elif fact.val.type == FUNCALL:
                funcs.append(fact)
            elif fact.val.type == SUMMATION:
                sums.append(fact)
            else:
                print('ERROR, unknown type for factor.val')
        vars.sort(key = lambda x: f'{x.val}')
        funcs.sort(key = lambda x: f'{x.val}')
        sums.sort(key = lambda x: f'{x.val}')
        new_const = Const.one()
        for fact in consts:
            new_const = new_const * fact.val.value

        new_vars = []
        prev_fact = None
        for fact in vars + [Factor()]:
            if prev_fact == None:
                prev_fact = fact
                continue
            else:
                if fact.val and fact.val.name == prev_fact.val.name:
                    prev_fact.exponent.value += fact.exponent.value
                else:
                    new_vars.append(prev_fact)
                    prev_fact = fact
        new_funcs = []
        prev_fact = None
        for fact in funcs + [Factor()]:
            if prev_fact == None:
                prev_fact = fact
            else:
                if fact.val and f'{fact}' == f'{prev_fact}':
                    prev_fact = frev_fact.deep_copy()
                    prev_fact.exponent.value += fact.exponent.value
                else:
                    new_funcs.append(prev_fact)
                    prev_fact = fact
        if new_const == Const.one() and (len(new_vars) + len(new_funcs) + len(sums)) > 0:
            self.factors = new_vars + new_funcs + sums
        else:
            const_factor = Factor()
            const_factor.val = Const(new_const)
            self.factors = [const_factor] + new_vars + new_funcs + sums

    #Term
    def expand_sums(self):
        # if any factors are expandable SUMMATIONs, return a list of expanded terms
        # if not, return False
        other_factors = []
        expandable_factors = []
        for factor in self.factors:
            if factor.val.type == SUMMATION and len(factor.val.terms) > 1 and factor.exponent.is_positive_integer():
                expandable_factors.append(factor)
            else:
                other_factors.append(factor)
        if len(expandable_factors) == 0:
            return False
        # first expand exponents
        sums_to_expand = []
        for factor in expandable_factors:
            for i in range(int(factor.exponent.value)):
                sum_to_expand = factor.val
                sums_to_expand.append(sum_to_expand)
        terms = []
        # need array of indexes to pick terms from each summation
        # and array of maximum values of indexes
        leni = len(sums_to_expand)
        indexes = [0 for i in range(leni)]
        max_indexes = [len(sum_to_expand.terms)-1 for sum_to_expand in sums_to_expand]
        def increment_indexes():
            for i in range(leni):
                if indexes[i] < max_indexes[i]:
                    indexes[i] += 1
                    return False
                else:
                    indexes[i] = 0
                    continue
            return True
        while True:
            term = Term()
            term.factors = [factor for factor in other_factors]
            for sum_index, term_index in enumerate(indexes):
                t = sums_to_expand[sum_index].terms[term_index].deep_copy()
                if t.sign < 0:
                    term.sign = -term.sign
                factors = [factor for factor in t.factors]
                term.factors += factors
            terms.append(term)
            done = increment_indexes()
            if done:
                break
        return terms

    #Term
    def set_const(self, const):
        xyxyxy(f'entering set_const, self is {self}, const is {const}')
        sign = 1
        if const < 0:
            sign = -1
            const = -const
        factor = Factor()
        factor.val = Const(const)
        self.sign = sign
        isone = const == Const.one()
        if len(self.factors) > 0 and self.factors[0].val.type == CONST:
            if isone:
                self.factors = self.factors[1:]
            else:
                self.factors[0] = factor
        else:
            if isone:
                pass
            else:
                self.factors = [factor] + self.factors
        xyxyxy(f'exiting set_const, self is {self}')

    #Term
    def like(self, oterm):
        i1 = 0
        if len(self.factors) > 0 and self.factors[0].val.type == CONST:
            i1 += 1
            const1 = self.factors[0].val.value
        else:
            const1 = Const.one()
        i2 = 0
        if len(oterm.factors) > 0 and oterm.factors[0].val.type == CONST:
            i2 += 1
            const2 = oterm.factors[0].val.value
        else:
            const2 = Const.one()
        # if number of remaining factors is not the same, not like terms
        if len(self.factors[i1:]) != len(oterm.factors[i2:]):
            return None
        while i1 < len(self.factors):
            factor1 = self.factors[i1]
            factor2 = oterm.factors[i2]
            if factor1.exponent.value == 3 and factor2.exponent.value == 3:
                xyxyxy(f'in like, term1 is {self}, term2 is {oterm}, const1 is {const1} sign1 is {self.sign}, const2 is {const2} sign2 is {oterm.sign}')
            if factor1.val.type != factor2.val.type:
                return None
            elif factor1.val.type == VAR:
                if factor1.val.name != factor2.val.name:
                    return None
                if factor1.exponent.value != factor2.exponent.value:
                    return None
            elif factor1.val.type == FUNCALL:
                if f'{factor1}' != f'{factor2}':
                    return None
            # if we get here both terms are compatible
            i1 += 1
            i2 += 1
        # terms are like, calculate new constant
        return self.sign * const1 + oterm.sign * const2
            
        

class Factor:
    def __init__(self):
        self.type = FACTOR
        self.sign = 1
        self.val = None
        self.exponent = Const(1)
    def __str__(self):
        result = ''
        if self.sign < 0:
            result += '(-'
        if self.val.type == SUMMATION:
            result += '('
        result += f'{self.val}'
        if self.val.type == SUMMATION:
            result += ')'
        if self.sign < 0:
            result += ')'
        if self.exponent.value != 1:
            result += f'^{self.exponent.value}'
        return result
    #Factor
    def deep_copy(self):
        factor = Factor()
        factor.sign = self.sign
        factor.val = self.val.deep_copy()
        factor.exponent = self.exponent.deep_copy()
        return factor

class Funcall:
    def __init__(self):
        self.type = FUNCALL
        self.funcname = None
        self.arglist = None
    def __str__(self):
        result = self.funcname
        if self.arglist:
            result += '('
            args = [arg.__str__() for arg in self.arglist]
            result += ','.join(args)
            result += ')'
        return result
    #Funcall
    def deep_copy(self):
        funcall = Funcall()
        funcall.funcname = self.funcname
        funcall.arglist = [arg.deep_copy() for arg in arglist]
        return funcall

class Const:
    def __init__(self, const):
        self.type = CONST
        self.value = Fraction(const)
    def __str__(self):
        return f'{self.value}'
    def deep_copy(self):
        const = Const(self.value)
        return const
    def is_positive_integer(self):
        return self.value.denominator == 1
    @staticmethod
    def one():
        return Fraction(1)
    @staticmethod
    def zero():
        return Fraction(0)

class Var:
    def __init__(self, name):
        self.type = VAR
        self.name = name
    def __str__(self):
        return self.name
    def deep_copy(self):
        return Var(self.name)

class Parser:
    def __init__(self, text):
        self.text = text
        self.idx = 0
        self.errored = False

    #Parser
    def error(self, msg):
        self.errored = True
        print(f'Error: {msg} at index {self.idx}')
        print(f'{self.text[:self.idx]} <<-->> {self.text[self.idx:]}')

    #Parser
    def rest(self):
        return self.text[self.idx:]

    #Parser
    def match(self, regexp):
        m = regexp.match(self.rest())
        if m:
            self.idx += m.end()
            return m.group(1)
        return False

    #Parser
    def parse_funcall(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_funcall, rest:{self.rest()}')
        funcall = Funcall()
        funcname = self.match(funcname_re)
        if funcname:
            funcall.funcname = funcname
        else:
            return False
        isparen = self.match(openparen_re)
        if isparen:
            arglist = self.parse_arglist()
            isparen = self.match(closeparen_re)
            if isparen:
                funcall.arglist = arglist
            else:
                self.error('illegal arglist')
                return False
        return funcall

    #Parser
    def parse_arglist(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_arglist, rest:{self.rest()}')
        args = []
        while True:
            var = self.parse_var()
            if var:
                args.append(var)
            else:
                break
            comma = self.match(comma_re)
            if comma:
                continue
            else:
                break
        if len(args) > 0:
            return args
        else:
            return False

    #Parser
    def parse_var(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_var, rest:{self.rest()}')
        result = False
        argname = self.match(var_re)
        if argname:
            result = Var(argname)
        return result

    #Parser
    def parse_val(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_val, rest:{self.rest()}')
        const = self.parse_const()
        if const:
            return const
        var = self.parse_var()
        if var:
            return var
        funcall = self.parse_funcall()
        if funcall:
            return funcall
        return False
        
    #Parser
    def parse_const(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_const, rest:{self.rest()}')
        const = self.match(const_re)
        if const:
            return Const(const)
        return False

    #Parser
    def parse_factor(self):
        factor = Factor()
        if self.errored:
            return False
        # xyxyxy(f'entering parse_factor, rest:{self.rest()}')
        result = False
        val = self.parse_val()
        if val:
            factor.val = val
        else:
            open_paren = self.match(openparen_re)
            if open_paren:
                summation = self.parse_summation()
                if not summation:
                    self.error('error parsing summation inside parentheses')
                    return False
                if not self.match(closeparen_re):
                    self.error('no closing parenthesis found')
                    return False
                factor.val = summation
            else:
                self.error('expecting val or open parenthesis')
                return False
        isexponent = self.match(exponent_re)
        if isexponent:
            exponent = self.parse_const()
            if exponent:
                factor.exponent = exponent
                return factor
            else:
                self.error('syntax error: no exponent found')
                return False
        else:
            return factor
        return False

    #Parser
    def parse_term(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_term, rest:{self.rest()}')
        term = Term()
        if self.match(minus_re):
            term.sign = -1
        while True:
            factor = self.parse_factor()
            if factor:
                term.factors.append(factor)
                if self.match(multiply_re):
                    continue
                else:
                    break
            else:
                self.error('expected factor')
                return False
        return term
        
    #Parser
    def parse_summation(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_summation, rest:{self.rest()}')
        summation = Summation()
        plus_or_minus = '+'
        while True:
            term = self.parse_term()
            if term:
                if plus_or_minus == '-':
                    term.sign = -1
                summation.terms.append(term)
                plus_or_minus = self.match(plus_or_minus_re)
                if plus_or_minus:
                    continue
                else:
                    return summation
            else:
                self.error('expected term')
                return False

    #Parser
    def parse_expr(self):
        if self.errored:
            return False
        # xyxyxy(f'entering parse_expr, rest:{self.rest()}')
        expr = Expression()
        open_paren = self.match(openbracket_re)
        expr.numerator = self.parse_summation()
        if not expr.numerator:
            self.error('expected summation expression')
            return False
        if open_paren:
            close_paren = self.match(closebracket_re)
            if not close_paren:
                self.error('missing close bracket')
                return False
        is_denom = self.match(slash_re)
        if is_denom:
            open_paren = self.match(openbracket_re)
            if not open_paren:
                self.error('missing open bracket in denominator')
                return False
            expr.denominator = self.parse_summation()
            if not expr.denominator:
                self.error('error in denominator')
                return False
            close_paren = self.match(closebracket_re)
            if not close_paren:
                self.error('missing close bracket in denominator')
                return False
        return expr
            

                
            
def xyxyxy(s):
    #print(s)
    return

def prt(parser,e):
    print(f'{parser.text} -----> {e}')

p = Parser('COS ( x )')
f = p.parse_funcall()
prt(p,f)

p = Parser('COS')
f = p.parse_funcall()
prt(p,f)

p = Parser('ATAN2 ( y, x )')
f = p.parse_funcall()
prt(p,f)

p = Parser('x ^ 2')
f = p.parse_factor()
prt(p,f)


p = Parser('- x ^ 2 * SIN * 3/4')
f = p.parse_term()
prt(p,f)

p = Parser('- 1/2 * x ^ 2 * SIN(x) + y ^ 2 * COS(x) - 3*x*y')
f = p.parse_summation()
prt(p,f)

p = Parser('[ -2 * x * SIN(x)^2 + y * COS(x)] / [x - 1]')
f = p.parse_expr()
prt(p,f)


p = Parser('-2 * x * (x + 1) * SIN(x)^2 + y * COS(x)')
f = p.parse_expr()
prt(p,f)

p = Parser(' (x + 1) * (x - 1) ')
f = p.parse_expr()
prt(p,f)
f.expand()
prt(p,f)
f.collect()
prt(p,f)

p = Parser(' (x + 1)*(x+1) ')
f = p.parse_expr()
prt(p,f)
f.expand()
prt(p,f)
f.collect()
prt(p,f)

p = Parser(' (x + 1)^3  ')
f = p.parse_expr()
prt(p,f)
f.expand()
prt(p,f)
f.collect()
prt(p,f)

p = Parser(' (x + 1)^3 * (x - 1) ')
f = p.parse_expr()
f.expand()
f.collect()
prt(p,f)

p = Parser(' (x + 1)^3 * (1 - x) ')
f = p.parse_expr()
f.expand()
f.collect()
prt(p,f)
