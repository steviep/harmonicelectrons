import re
from fractions import Fraction

var_re = re.compile(" *([a-z][0-9]?)")
const_re = re.compile(' *(\d+(/\d+)?)')
add_re = re.compile(' *([+-])')
sign_re = re.compile(' *([+-])')
exp_re = re.compile(' *(\^)')
func_re = re.compile(' *([A-Z]+)')
multop_re = re.compile(' *([*/])')
openparen_re = re.compile(' *(\()')
closeparen_re = re.compile(' *(\))')

VAR = 'VAR'
CONST = 'CONST'
FUNCALL = 'FUNCALL'
EXPRESSION = 'EXPRESSION'
ZERO = 0

rules = {
    'SIN' : 'COS x',
    'COS' : '-SIN x'
}

class Factor:
    def __init__(self):
        self.base = ''
        self.numerator = True
        self.type = None
        self.funcname = None
        self.argument = None
        self.exponent = 1
        self.expr = None
        self.negative = False

    def __str__(self):
        result = ''
        if self.type == CONST:
            if self.base.denominator == 1:
                result = str(self.base.numerator)
            else:
                result = str(self.base.numerator) + '/' + str(self.base.denominator)
        elif self.type == VAR:
            result = self.base
        elif self.type == FUNCALL:
            result = self.funcname
            if self.argument != None:
                result += f'({self.argument})'
        elif self.type == EXPRESSION:
            result = f'({self.expr})' 
        if self.exponent != 1:
            result += '^' + str(self.exponent)
        return result
        
    # Factor
    def expandable(self):
        return self.type == EXPRESSION and self.exponent.denominator == 1

    # Factor
    def ddx(self, var):
        if self.type == CONST:
            return ZERO
        elif self.type == VAR:
            if self.base == var:
                const = Factor()
                const.type = CONST
                const.base = self.exponent
                
                factor = Factor()
                factor.type = VAR
                factor.base = var
                factor.exponent = self.exponent - 1
                if factor.exponent == 0:
                    return [const]
                else:
                    return [const, factor]
            else:
                return ZERO
        elif self.type == FUNCALL:
            if self.argument == None:
                result = Factor()
                result.type = FUNCALL
                result.funcname = f'd/d{var}.{self.funcname}'
                result.argument = None
                return [result]
            else:
                if self.argument == var:
                    rule = rules.get(self.funcname, None)
                    if rule:
                        if type(rule) == str:
                            expr = Expr(rule).expr
                            rule = expr.terms[0].factors[0]
                            rule.negative = not expr.terms[0].added
                            rules[self.funcname] = rule
                        result = rule.dup()
                        result.argument = var
                    else:
                        result = Factor()
                        result.type = FUNCALL
                        result.funcname = f'd/d{var}.{self.funcname}'
                        result.argument = var
                    return [result]
                else:
                    return ZERO
        elif self.type == EXPRESSION:
            result = []
            const = Factor()
            const.type = CONST
            const.base = self.exponent
            result.append(const)
            
            factor = Factor()
            factor.type = EXPRESSION
            factor.expr = self.expr
            factor.exponent = self.exponent - 1
            if factor.exponent != 0:
                result.append(factor)

            factor = Factor()
            factor.type = EXPRESSION
            factor.expr = self.expr.ddx(var, True)
            if factor.expr == ZERO:
                return ZERO
            result.append(factor)
            return result
        else:
            print(f'should never happen')
            return ZERO

    #Factor
    def dup(self):
        factor = Factor()
        factor.base = self.base
        factor.numerator = self.numerator
        factor.type = self.type
        factor.funcname = self.funcname
        factor.argument = self.argument
        factor.exponent = self.exponent
        factor.expr = self.expr
        factor.negative = self.negative
        return factor

    #Factor
    def flip(self):
        self.numerator = not self.numerator


class Term:
    def __init__(self):
        self.added = True
        self.text = ''
        self.factors = []

    #Term
    def ddx(self, var):
        ddxterms = []
        for i,factor in enumerate(self.factors):
            term = Term()
            term.added = self.added
            term.factors = [factor.dup() for factor in self.factors]
            ddxfactors = factor.ddx(var)
            if ddxfactors == ZERO:
                continue
            for factor in ddxfactors:
                if factor.negative:
                    term.flip()
                    factor.negative = False
            term.factors = term.factors[:i] + ddxfactors + term.factors[i+1:]
            ddxterms.append(term)
        return ddxterms

    #Term
    def flip(self):
        self.added = not self.added

    #Term
    def set_const(self, const):
        if len(self.factors) == 0 or self.factors[0].type != CONST:
            factor = Factor()
            factor.type = CONST
            self.factors = [factor] + self.factors
        if const < 0:
            self.added = False
            const = -const
        else:
            self.added = True
        self.factors[0].base = const

    #Term
    def const_present(self):
        if len(self.factors) > 0 and self.factors[0].type == CONST:
            const = self.factors[0].base
            found = True
        else:
            const = Fraction(1)
            found = False
        return (found, const)

    #Term
    def like(self, oterm):
        # assume both terms are simplified
        const_present, const1 = self.const_present()
        if const_present:
            i1 = 1
        else:
            i1 = 0
        const_present, const2 = oterm.const_present()
        if const_present:
            i2 = 1
        else:
            i2 = 0
        if len(self.factors[i1:]) != len(oterm.factors[i2:]):
            return None
        while i1 < len(self.factors):
            factor1 = self.factors[i1]
            factor2 = oterm.factors[i2]
            if factor1.type != factor2.type:
                return None
            if factor1.type == VAR:
                if factor1.base != factor2.base or factor1.exponent != factor2.exponent:
                    return None
            else:
                if factor1.funcname != factor2.funcname:
                    return None
            # if we get here both terms are still compatible
            i1 += 1
            i2 += 1
        # terms are like, calculate new constant
        if not self.added:
            const1 = -const1
        if not oterm.added:
            const2 = -const2
        return const1 + const2

    #Term
    # if factors are expressions with multiple terms with positive integer exponents,
    # do the expansion and return an expression.  Otherwise return False
    def expand(self):
        simple_factors = [fact for fact in self.factors if not fact.expandable()]
        expr_factors = [fact for fact in self.factors if fact.expandable()]
        exprs = []
        for fact in expr_factors:
            for i in range(fact.exponent.numerator):
                exprs.append(fact.expr)
        # (x^2 + 3*x + 1) * (2*x - 2) * (x^3 + x + 2)
        # i = [  i0,           i1,          i2      ]
        leni = len(exprs)
        indexes = [0 for i in range(leni)]
        maxes = [len(expr.terms)-1 for expr in exprs]
        expr = Expression()
        expr.text = f'{self}'
        def increment_indexes():
            for i in range(leni):
                if indexes[i] < maxes[i]:
                    indexes[i] += 1
                    return False
                else:
                    indexes[i] = 0
                    continue
            return True
        while True:
            term = Term()
            term.factors = [factor.dup() for factor in simple_factors]
            for expr_index, term_index in enumerate(indexes):
                t = exprs[expr_index].terms[term_index]
                if not t.added:
                    term.flip()
                factors = [factor.dup() for factor in t.factors]
                term.factors += factors
            expr.terms.append(term)
            done = increment_indexes()
            if done:
                break
        return expr


    # Term
    def simplify(self):
        factors = []
        for fact in self.factors:
            if fact.type == EXPRESSION:
                term = fact.expr.isterm()
                if term:
                    factors += term.factors
                else:
                    factors.append(fact)
            else:
                factors.append(fact)
        self.factors = factors
        consts = []
        vars = []
        funcs = []
        exprs = []

        for fact in self.factors:
            if fact.type == CONST:
                consts.append(fact)
            elif fact.type == VAR:
                vars.append(fact)
            elif fact.type == FUNCALL:
                funcs.append(fact)
            elif fact.type == EXPRESSION:
                exprs.append(fact)
            else:
                print("ERROR, unknown type for factor")
        vars.sort(key = lambda x: x.base)
        funcs.sort(key = lambda x: x.funcname)
        simplifieds = []
        result = Fraction(1)
        if len(consts) > 0:
            for const in consts:
                if const.numerator:
                    result = result * const.base
                else:
                    result = result / const.base
        new_const = Factor()
        new_const.type = CONST
        new_const.base = result
        new_vars = []
        prev_var = None
        # dummy is put at the end to trigger the append of last var
        dummy = Factor()
        for var in vars + [dummy]:
            if prev_var == None:
                prev_var = var
            else:
                if var.base == prev_var.base:
                    prev_var.exponent += var.exponent
                else:
                    new_vars.append(prev_var)
                    prev_var = var
        if new_const.base == Fraction(1):
            self.factors = new_vars + funcs + exprs
        else:
            self.factors = [new_const] + new_vars + funcs + exprs

    #Term
    def prtvars(self, vars):
        print(f'vars =', end = '')
        for var in vars:
            print(f'{var} ', end = '')
        print()

    #Term
    def dup(self):
        term = Term()
        term.added = self.added
        term.text = self.text
        term.factors = self.factors
        return term

    #Term
    def __str__(self):
        if len(self.factors) == 0:
            return '1'
        result = ''
        for i,factor in enumerate(self.factors):
            if i == 0:
                if not factor.numerator:
                    result += '1/'
            if i > 0:
                if factor.numerator:
                    result += '*'
                else:
                    result += '/'
            result += factor.__str__()
        return result
        
class Expr:
    def __init__(self, text):
        e = Expression()
        e.text = text
        e.parse_expression()
        self.expr = e

class Expression:
    def __init__(self):
        self.terms = []
        self.text = None
        self.idx = 0
        self.error = False

    # grammar:
       # expr = term [ '+'|'-' expr]
       # term = factor [ '*'|'/' term]
       # factor = val | val '^' const
       # val = var | const | func_call | '(' expr ')'
       # func_call = FUNC | FUNC val

    #Expression
    def dup(self):
        expr = Expression()
        expr.terms = self.terms
        expr.text = self.text
        expr.idx = self.idx
        expr.error = self.error

    #Expression
    def ddx(self, var, prt=False):
        deriv = Expression()
        deriv.text = f'd/d{var} {self.text}'
        deriv.terms = []
        for term in self.terms:
            ddxterm = term.ddx(var)
            if ddxterm != ZERO:
                deriv.terms += ddxterm
        deriv.collect()
        return deriv

    #Expression
    def error(self):
        print(f'error parsing {self.text} of len {len(self.text)} at position {self.idx}')
        self.error = True
        
    #Expression
    def isterm(self):
        if len(self.terms) == 1:
            term = Term()
            term.factors = self.terms[0].factors
            return term
        return False

    #Expression
    def expand(self):
        newterms = []
        for term in self.terms:
            e = term.expand()
            newterms += e.terms
        self.terms = newterms
        return self

    #Expression
    def collect(self):
        self.simplify()
        # collect like terms
        for i1,term1 in enumerate(self.terms):
            for i2 in range(i1+1, len(self.terms)):
                term2 = self.terms[i2]
                if term1._deleted or term2._deleted:
                    continue
                islike = term1.like(term2)
                if islike == None:
                    continue
                else:
                    newconst = islike
                    if newconst == Fraction(0):
                        term1._deleted = True
                        term2._deleted = True
                    else:
                        term1.set_const(newconst)
                        term2._deleted = True
        self.terms = [term for term in self.terms if not term._deleted]

    #Expression
    def simplify(self):
        for term in self.terms:
            term._deleted = False
            term.simplify()

    #Expression
    def rest(self):
        return self.text[self.idx:]

    #Expression
    def parse_expression(self, added = True):
        if self.text == None:
            return self
        term = Term()
        term = self.parse_term(term)
        if term:
            if not added:
                term.flip()
            self.terms.append(term)
        else:
            return None
        op = self.additive_op()
        if op:
            self.parse_expression(op == '+')
        return self

    #Expression
    def match(self, regexp):
        m = regexp.match(self.rest())
        if m:
            self.idx += m.end()
            return m.group(1)
        return None

    # consume a variable return variable name if success
    #Expression
    def var(self):
        return self.match(var_re)

    #Expression
    def mult_op(self):
        return self.match(multop_re)

    #Expression
    def parse_term(self, term, mult_op = '*'):
        factor = Factor()
        start = self.idx
        sign = self.match(sign_re)
        if sign == '-':
            term.added = False
        factor = self.parse_factor(factor)
        if factor:
            if mult_op == '/':
                factor.flip()
            term.factors.append(factor)
        op = self.mult_op()
        if op:
            self.parse_term(term, op)
        term.text = self.text[start:self.idx]
        return term
            
    #Expression
    def parse_factor(self, factor):
        found = False
        start = self.idx
        if not found:
            c = self.match(const_re)
            if c != None:
                found = True
                factor.type = CONST
                factor.base = Fraction(c)
        if not found:
            var = self.var()
            if var:
                found = True
                factor.type = VAR
                factor.base = var
        if not found:
            f = self.match(func_re)
            if f:
                factor.type = FUNCALL
                factor.funcname = f
                argument = Term()
                legal = self.parse_term(argument)
                if legal:
                    factor.argument = f'{argument}'
                else:
                    factor.argument = None
                found = True
        if not found:
            f = self.match(openparen_re)
            if f:
                expr = Expr(self.rest()).expr
                self.idx += expr.idx
                f = self.match(closeparen_re)
                if f:
                    factor.type = EXPRESSION
                    factor.expr = expr
                    found = True
        if not found:
            self.error()
            return None
        exp_op = self.match(exp_re)
        if exp_op:
            c = self.match(const_re)
            if c != None:
                factor.exponent = Fraction(c)
        return factor

    #Expression
    def additive_op(self):
        return self.match(add_re)

    #Expression
    def prt(self):
        print(f'{self.text} ------> {self}')

    #Expression
    def __str__(self):
        if len(self.terms) == 0:
            return '0'
        result = ''
        for i, term in enumerate(self.terms):
            if i == 0:
                if not term.added:
                    result += '-'
            if i > 0:
                if term.added:
                    result += '+'
                else:
                    result += '-'
            result += term.__str__()
        return result

e = Expr("23").expr
e.prt()
e = Expr("x").expr
e.prt()
e = Expr("x + y").expr
e.prt()
e = Expr("-x1 + x2 + 1").expr
e.prt()
e = Expr("3/4*x - x + 2*y + 3*z + 1/4*x").expr
e.prt()
e.collect()
e.prt()
e = Expr("x * y + 3 * z^2 / 5").expr
e.prt()
e.collect()
e.prt()
e = Expr("2*x").expr
e.simplify()
e.prt()
e = Expr("x * 4/3 * y * 2 * x - 3 * z^2 / 5").expr
e.prt()
e.simplify()
e.prt()
f = e.ddx('x')
f.prt()
f = f.ddx('x')
f.prt()
e = Expr("COS x").expr
e.prt()
f = e.ddx('x')
f.prt()
e = Expr("y * COS y").expr
e.prt()
f = e.ddx('y')
f.prt()
g = f.ddx('y')
g.prt()
e = Expr("COS y - y * SIN y").expr
f = e.ddx('y')
f.prt()
e = Expr("(x^2 + y^2)^1/2").expr
e.prt()
f = e.ddx('x')
f.prt()
f = e.ddx('y')
f.prt()
e = Expr("(x^2 + y^2)^3/2").expr
e.prt()
f = e.ddx('x')
f.prt()
f = e.ddx('y')
f.prt()
e = Expr("x*1").expr
e.simplify()
e.prt()
e = Expr("(x+1) * (x-1)").expr
e.expand()
e.collect()
e.prt()
e = Expr("(x-1)^3").expr
e.expand()
e.collect()
e.prt()
