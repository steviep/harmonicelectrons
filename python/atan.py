from math import pi, atan, sin, cos
import math

def atan2(y,x):
    if x < 0:
      if y < 0:
          return -pi + atan(y/x)
      elif y > 0:
          return pi + atan(y/x)
      else:  # y == 0
          return pi
    elif x > 0:
        return atan(y/x)
    else:  # x == 0
        if y < 0:
            return -pi/2
        elif y > 0:
            return pi/2
        else:
            return 0

iters = 20
# for i in range(iters):
#     angle = 2 * pi * i / iters
#     x = cos(angle)
#     y = sin(angle)
#     result = atan2(y,x)
#     print(f'angle = {angle}, atan = {result}, diff = {abs(angle - result)}')

def diff(a,b):
    if a == 0 and b == 0:
        return 0
    m = max(abs(a), abs(b))
    return abs((a-b) * 100 / m)
    

def check_agrees(x,y):
    a = atan2(y,x)
    b = math.atan2(y,x)
    d = diff(a,b)
    print(f'my atan {a:1.5f}, real atan {b:1.5f}, diff {diff(a,b):1.14f}')

def prt(x,y):
    # a = atan2(y,x)
    # print(f'({x:1.2f},{y:1.2f}) atan is {a/pi:1.5f} pi')
    #check_yderiv(x,y)
    check_agrees(x,y)

def datan_dx(x,y):
    r2 = x*x + y*y
    if r2 == 0:
        return 0
    return -y/r2

def datan_dy(x,y):
    r2 = x*x + y*y
    if r2 == 0:
        return 0
    return x/r2

def check_xderiv(x,y):
    fxy = atan2(y,x)
    dx = .0001
    dfdx = (atan2(y, x+dx) - fxy)/dx
    dfdxm = -(atan2(y, x-dx) - fxy)/dx
    average = (dfdx + dfdxm)/2
    calculated = datan_dx(x,y)
    print(f'average dx deriv is {average:1.5f}, calculated {calculated:1.5f}, %diff is {diff(average,calculated):1.8f}')
    #print(f'average dx deriv is {average}, calculated {calculated}, %diff is {diff(average,calculated)}')

def check_yderiv(x,y):
    fxy = atan2(y,x)
    dy = .0001
    dfdy = (atan2(y+dy, x) - fxy)/dy
    dfdym = -(atan2(y-dy, x) - fxy)/dy
    average = (dfdy + dfdym)/2
    calculated = datan_dy(x,y)
    print(f'average dy deriv is {average:1.5f}, calculated {calculated:1.5f}, %diff is {diff(average,calculated):1.8f}')
    #print(f'average dy deriv is {average}, calculated {calculated}, %diff is {diff(average,calculated)}')
    
    

x = 1
r = range(iters+1)
for i in r:
    y = i / iters
    prt(x,y)
for i in r:
    x = 1 - i/iters
    prt(x,y)
for i in r:
    x = -i/iters
    prt(x,y)
for i in r:
    y = 1 - i/iters
    prt(x,y)
for i in r:
    y = -i/iters
    prt(x,y)
for i in r:
    x = -1 + i/iters
    prt(x,y)
for i in r:
    x = i/iters
    prt(x,y)
for i in r:
    y = -1 + i/iters
    prt(x,y)
        
